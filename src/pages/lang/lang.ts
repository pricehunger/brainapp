import { Component,OnInit} from '@angular/core';
import { NavController,IonicPage,NavParams } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';
import {UserService} from '../../services/user.service';
import { AlertController } from 'ionic-angular';
import {AppConstants} from '../../shared/app.constants';
import { ImageViewerController } from 'ionic-img-viewer';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';

@IonicPage()
@Component({
  selector: 'page-lang',
  templateUrl: 'lang.html',
  providers: [
    PostService,
    UserService
  ]
})

export class LangPage implements OnInit{
  lang: Post;
  userFavorites: any[]=[];
  result:any= null;
  searchKeyword: string;
  showAnswer: boolean= false;

  constructor(public navCtrl: NavController, public navParams: NavParams,private sanitizer: DomSanitizer,private userService: UserService,private alertCtrl: AlertController,postService: PostService,private imageViewerCtrl: ImageViewerController) {
    this.lang= this.navParams.get('post');
    let loadFromServer= this.navParams.get('loadFromServer');
    
    if(loadFromServer){
       postService.getPost(this.lang.id,'lang').subscribe(lang=>{
         this.lang= lang;
       });
    }
  }

  ngOnInit(){
      this.userService.getUser().then(user=>{
        this.userFavorites= user.favorites.toString().split(',').map(Number);
      });
  }

 
 showAlert(msg: string,title:string="Error") {
  const alert = this.alertCtrl.create({
   title: title,
   subTitle: msg,
   buttons: ['Dismiss']
  });
  alert.present();
 }

 checkForResponseError(err: any){ 
  try{
    let errorResponse= JSON.parse(err._body);
    if(errorResponse.code=="rest_post_invalid_page_number")
      return;
    if(errorResponse.error=="session_expired"){
        this.showAlert("Seems your session is expired. Please login again.");
       // this.navCtrl.push(LoginPage);
        this.userService.logout();
    }
    else
    this.showAlert(errorResponse.error);   
  }  catch(error){
    this.showAlert(AppConstants.JSON_ERROR); 
  }
}

getAnswer(){
  this.showAnswer= true;
}

updateFavorites(){
  this.userService.getAuthCookie().then(val=>{
  this.userService.saveFavorites(this.userFavorites,val).subscribe((data)=>{
      if(data.hasOwnProperty('user')){
          this.userService.saveUser(data.user);
      }
    },err=>{
      this.checkForResponseError(err);
    });
  })
}

removeFavorite(id: number){
  let index= this.userFavorites.indexOf(id);
  if(index>=0)
  { 
    this.userFavorites.splice(index, 1);
    this.updateFavorites();
  }
}

addFavorite(id: number){
  this.userFavorites.push(id);
  this.updateFavorites();
  
}


}
