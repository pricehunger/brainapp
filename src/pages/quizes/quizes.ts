import { Component,OnInit, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QuizPage } from '../quiz/quiz';
import { QuizService } from '../../services/quiz.service';

@Component({
  selector: 'page-quizes',
  templateUrl: 'quizes.html',
  providers:[QuizService]
})
export class QuizesPage implements OnInit{
    quizes: any= [];
    
    constructor(public navCtrl: NavController,public qs: QuizService) {
 
    }

    openQuiz(quiz){
      this.navCtrl.push(QuizPage,{'quiz': quiz});
    }

    ngOnInit(){
      this.qs.getQuizes().subscribe((quizes)=>this.quizes= quizes);
    }
}
