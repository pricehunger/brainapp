import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElPlusPage } from './el-plus';

@NgModule({
  declarations: [
    ElPlusPage,
  ],
  imports: [
    IonicPageModule.forChild(ElPlusPage)
  ],
  providers:[]
})
export class ElPlusPageModule {}
