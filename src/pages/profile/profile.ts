import { Component,OnInit,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,App } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {FormGroup,FormBuilder,Validators, RequiredValidator} from '@angular/forms';
import {User} from '../../models/user.model';
import { Nav } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import {AppConstants} from '../../shared/app.constants';
import { ValidateEmail } from '../../shared/validators/email.validator';
import {LoginPage} from '../login/login';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [
    UserService
  ]
})

export class ProfilePage implements OnInit{
  profileForm: FormGroup;
  requestInProgress: boolean= false;
  countries: any[]= [];
  constructor(private alertCtrl: AlertController,private storage: Storage ,public navCtrl: NavController, public navParams: NavParams,private userService: UserService,private fb: FormBuilder,public viewCtrl: ViewController,public appCtrl: App) {
  }

  
  ngOnInit(){
    this.countries = AppConstants.countries;
    this.profileForm= this.fb.group({
      firstname: [''],
      lastname: [''],
      email: ['',Validators.compose([ValidateEmail])],
      mobile: [''],
     // country: ['',Validators.required]
    });

    this.userService.getUser().then(user=>{
        this.initializeData(user)
    });
  }

  initializeData(user: User){
    this.profileForm.controls.firstname.setValue(user.firstname);
    this.profileForm.controls.lastname.setValue(user.lastname);
    this.profileForm.controls.email.setValue(user.email);
    this.profileForm.controls.mobile.setValue(user.mobile);
    //this.profileForm.controls.country.setValue(user.countryCode);
  }

  showAlert(msg: string,title:string="Error",error : boolean= false) {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: [
                {
                  text: 'Dismiss',
                  role: 'dismiss',
                  handler: () => {
                    if(!error){
                      this.navCtrl.setRoot(HomePage);
                    }
                  }
                }
              ]
    });
    alert.present();
  }

  checkForResponseError(err: any){
    try{
      let errorResponse= JSON.parse(err._body);
      if(errorResponse.error=="session_expired"){
          this.showAlert("Seems your session is expired. Please login again.");
          this.navCtrl.push(LoginPage)
          this.userService.logout();
      }
      else
      this.showAlert(errorResponse.error,'Error',true);   
    }  catch(error){
      this.showAlert(AppConstants.JSON_ERROR,'Error',true); 
    }
  }
  
  showRequestInProgress(status: boolean){
    this.requestInProgress= status;
  }

  update(valid){
    if(!valid)
        return;
    this.showRequestInProgress(true);
    this.storage.get("authCookie").then(val=>{
      this.userService.updateUser(this.profileForm.value,val).subscribe((data)=>{
        this.showRequestInProgress(false);
        if(data.status=="ok"){
          this.userService.saveUser(data.user);
          this.showAlert("Profile updated successfully","Profile Update"); 
        }
      },err=>{this.showRequestInProgress(false); this.checkForResponseError(err); })
    })    
     
  }
}
