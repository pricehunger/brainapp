import { Component,OnInit } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {QA} from '../../models/qa.model';
import {ElPlusPage} from '../el-plus/el-plus';
import {PostService} from '../../services/post.service';
import { AlertController } from 'ionic-angular';
import {AppConstants} from '../../shared/app.constants';

@Component({
  selector: 'page-gks',
  templateUrl: 'el-plus-list.html',
  providers:[
    PostService,
    UserService
  ]
})
export class ElPlusListPage implements OnInit  {
  
  user:User;
  posts: QA[]=[];
  searchKeyword: string;
  requestInProgress: boolean= false;
  suggestions: QA[]=[];
  userFavorites: any[]=[];
  postType:string='11_plus';
  taxonomy:string;
  taxonomyValue:number;
  heading: string;

  constructor(private navCtrl: NavController,private service: PostService,private alertCtrl: AlertController,private userService: UserService,private navParams: NavParams) {
    this.taxonomy = navParams.get('type');
    this.taxonomyValue = navParams.get('typeValue');
    this.heading = navParams.get('heading');
  }

  ngOnInit(){ 
    this.userService.getUser().then(user=>{
      if(user && user.favorites)
        this.userFavorites= user.favorites.toString().split(',').map(Number);
    });
    this.requestInProgress=true;
    this.service.getPostsByTaxonomy(1,this.postType,this.taxonomy,this.taxonomyValue).finally(()=>this.requestInProgress=false)
                                 .subscribe((posts:QA[])=> this.posts= posts);
  }

  doInfinite(infiniteScroll) {
    let page: number= Math.ceil(this.posts.length/5) + 1; 
    this.service.getPostsByTaxonomy(page,this.postType,this.taxonomy,this.taxonomyValue).subscribe((gks:QA[])=> { this.posts= this.posts.concat(gks); infiniteScroll.complete() },
    err=> {infiniteScroll.complete(); this.checkForResponseError(err);}
    );
   }

   checkForResponseError(err: any){ 
    try{
      let errorResponse= JSON.parse(err._body);
      if(errorResponse.code=="rest_post_invalid_page_number")
        return;
      if(errorResponse.error=="session_expired"){
          this.showAlert("Seems your session is expired. Please login again.");
          //this.navCtrl.push(LoginPage);
          this.userService.logout();
      }
      else
      this.showAlert(errorResponse.error);   
    }  catch(error){
       this.showAlert(AppConstants.JSON_ERROR); 
    }
  }

  showAlert(msg: string,title:string="Error") {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: ['Dismiss']
    });
    alert.present();
   }

   openDetailPage(qa: QA){
    this.navCtrl.push(ElPlusPage,{'post': qa});
  }

  searchPost(){
    if(this.searchKeyword.length<3){
     this.clearSearch();
     return false;
    }
       
    this.service.searchPosts(this.searchKeyword,'11_plus').subscribe((posts:QA[])=>{
       this.suggestions= posts;
    },err=> this.checkForResponseError(err))
  }
 
  onInput(event: any){
     this.searchPost();
  }
 
  clearSearch(){
    this.suggestions= [];
  }


addFavorite(id: number){
  this.userFavorites.push(id);
  this.updateFavorites();
  
}

updateFavorites(){
 this.userService.getAuthCookie().then(val=>{
 this.userService.saveFavorites(this.userFavorites,val).subscribe((data)=>{
     if(data.hasOwnProperty('user')){
         this.userService.saveUser(data.user);
     }
   },err=>{
     this.checkForResponseError(err);
   });
 })
}

removeFavorite(id: number){
 let index= this.userFavorites.indexOf(id);
 if(index>=0)
 { 
   this.userFavorites.splice(index, 1);
   this.updateFavorites();
 }
}

}
