import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElPlusListPage } from './el-plus-list';
import { ElPlusPageModule } from '../el-plus/el-plus.module';

@NgModule({
  declarations: [
    ElPlusListPage
  ],
  imports: [
    ElPlusPageModule,
    IonicPageModule.forChild(ElPlusListPage),
  ],
  providers: []
})
export class ElPlusListPageModule {}
