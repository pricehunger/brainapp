import { Component,OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {Trick} from '../../models/trick.model';
import {PostService} from '../../services/post.service';
import { AlertController } from 'ionic-angular';
import {AppConstants} from '../../shared/app.constants';
import {TrickPage} from '../trick/trick';

@Component({
  selector: 'page-maths',
  templateUrl: 'maths.html',
  providers:[
    PostService,
    UserService
  ]
})
export class MathsPage implements OnInit {
  user:User;
  tricks: Trick[]=[];
  trickSuggestions: Trick[]=[];
  searchKeyword: string;
  userFavorites: any[]=[];
  requestInProgress: boolean= false;

  constructor(private userService: UserService,private navCtrl: NavController,private service: PostService,private alertCtrl: AlertController) {
  }

  ngOnInit(){ 
    this.userService.getUser().then(user=>{
      if(user && user.favorites)
        this.userFavorites= user.favorites.toString().split(',').map(Number);
    });
    this.requestInProgress=true;
    this.service.getPosts().finally(()=>this.requestInProgress=false)
                                 .subscribe(tricks=> this.tricks= tricks,
                                            error=>this.checkForResponseError(error));
    
  }

  showAlert(msg: string,title:string="Error") {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: ['Dismiss']
    });
    alert.present();
   }
  
  checkForResponseError(err: any){ 
    try{
      let errorResponse= JSON.parse(err._body);
      if(errorResponse.code=="rest_post_invalid_page_number")
        return;
      if(errorResponse.error=="session_expired"){
          this.showAlert("Seems your session is expired. Please login again.");
          //this.navCtrl.push(LoginPage);
          this.userService.logout();
      }
      else
      this.showAlert(errorResponse.error);   
    }  catch(error){
       this.showAlert(AppConstants.JSON_ERROR); 
    }
  }

 openDetailPage(trick: Trick){
   this.navCtrl.push(TrickPage,{'trick': trick});
 }

 searchTrick(){
   if(this.searchKeyword.length<3){
    this.clearSearch();
    return false;
   }
      
   this.service.searchPosts(this.searchKeyword).subscribe(tricks=>{
      this.trickSuggestions= tricks;
   },err=> this.checkForResponseError(err))
 }

 onInput(event: any){
    this.searchTrick();
 }

 clearSearch(){
   this.trickSuggestions= [];
 }
 openTrickDetail(trick: Trick){
     this.navCtrl.push(TrickPage,{'trick': trick});
 }

  doInfinite(infiniteScroll) {
   let page: number= Math.ceil(this.tricks.length/5) + 1; 
   this.service.getPosts(page).subscribe(tricks=> { this.tricks= this.tricks.concat(tricks); infiniteScroll.complete() },
   err=> {infiniteScroll.complete(); this.checkForResponseError(err);}
   );
  }

  addFavorite(id: number){
    this.userFavorites.push(id);
    this.updateFavorites();
    
 }

 updateFavorites(){
   this.userService.getAuthCookie().then(val=>{
   this.userService.saveFavorites(this.userFavorites,val).subscribe((data)=>{
       if(data.hasOwnProperty('user')){
           this.userService.saveUser(data.user);
       }
     },err=>{
       this.checkForResponseError(err);
     });
   })
 }
 
 removeFavorite(id: number){
   let index= this.userFavorites.indexOf(id);
   if(index>=0)
   { 
     this.userFavorites.splice(index, 1);
     this.updateFavorites();
   }
 }

}
