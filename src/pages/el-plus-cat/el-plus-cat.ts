import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,AlertController,LoadingController } from 'ionic-angular';
import {ElPlusListPage} from '../el-plus-list/el-plus-list';
import { QuizPage } from '../quiz/quiz';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { PostService } from '../../services/post.service';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-el-plus-cat',
  templateUrl: 'el-plus-cat.html',
  providers: [PostService]
})

export class ElPlusCatPage{
  type:string;
  plans;
  last={ID:"",name:""};
  is_subscribe=false;
  constructor(public navCtrl: NavController,
    public storage:Storage,
    public navParams: NavParams,private iap: InAppPurchase,public postService:PostService,
  public platform:Platform,
  private alertCtrl: AlertController,
  public loadingCtrl: LoadingController) {
    this.type= this.navParams.get('type');
    
     this.getPlans()
  }

  getPlans(){
    let loader=this.presentLoadingDefault();
    this.storage.get('userData').then(user=>{
      this.postService.getMyPlan( user.id).subscribe(data=>{
        this.is_subscribe= data.data.is_plan_active; 
      });
 

    }); 
    this.iap
      .getProducts(['com.prod1'])
      .then((products) => {
       // console.log(products);
        this.plans=products
        loader.dismiss()
          //  [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', price: '...' }, ...]
      })
      .catch((err) => {
        alert(err)
        loader.dismiss()
        console.log(err);
      });
  }

  buy(prod){
console.log(prod);
    this.iap
  .subscribe(prod.productId)
  .then((data)=> {
    let loader=this.presentLoadingDefault();
    console.log(data);
    console.log(data,"data");
    this.storage.get('userData').then(user=>{
      this.postService.subscribePlan(data.signature,user.id,data.receipt,data.transactionId,prod.productId,prod.price,this.platform.is("android")?"ANDROID":"IOS").subscribe(data=>{
        if(data.success){
          this.is_subscribe= true;
           this.openQuizPage(this.last.ID,this.last.name);
        }
        let alert = this.alertCtrl.create({
          title: "Success",
          subTitle: data.data,
          buttons: ['Dismiss']
        });
        alert.present();
        loader.dismiss()
      },err=>{
        loader.dismiss()
      });
 

    });
    // {
    //   transactionId: ...
    //   receipt: ...
    //   signature: ...
    // }
  })
  .catch((err)=> {
    console.log(err);
  });

  }
  openElPlusPage(typeValue,heading){
    this.navCtrl.push(ElPlusListPage,{type: this.type,typeValue: typeValue,heading:heading});
  }

  openQuizPage(id,quizName){
    if(!this.is_subscribe){
      this.last= {'ID': id,'name': quizName};;
      this.presentConfirm();
    }
    else{
      let quiz= {'ID': id,'name': quizName};
      this.navCtrl.push(QuizPage,{'quiz': quiz});
    }
  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title:this.plans[0].title,
      message:  this.plans[0].description+"<br> Monthly subscription "+this.plans[0].price,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: () => {
            this.buy(this.plans[0])
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.setMode("ios");
    alert.present();
    
  }
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
  
    loading.present();
  
   return loading;
  } 
  
  
 
}
