import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElPlusCatPage } from './el-plus-cat';
import {ElPlusListPageModule} from '../el-plus-list/el-plus-list.module';

@NgModule({
  declarations: [
    ElPlusCatPage
  ],
  imports: [
    ElPlusListPageModule,
    IonicPageModule.forChild(ElPlusCatPage),
  ],
  providers: []
})
export class ElPlusCatPageModule {}
