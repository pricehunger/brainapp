import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {LoginPage} from '../login/login';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
  ],
  providers: [
    UserService
  ]
})
export class LoginPageModule {}
