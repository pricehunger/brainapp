import {Component,OnInit} from '@angular/core';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import { NavController } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import { AlertController } from 'ionic-angular';
import {User} from '../../models/user.model';
import {HomePage} from '../../pages/home/home';
import {Sim} from '@ionic-native/sim';
import { AppConstants } from '../../shared/app.constants';
import { ProfilePage } from '../profile/profile';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { jsonpFactory } from '../../../node_modules/@angular/http/src/http_module';

@Component({
    selector: 'user-login',
    templateUrl: 'login.html',
    providers: [UserService,Sim, GooglePlus, Facebook]
})

export class LoginPage implements OnInit{
public static JSON_ERROR= "Unable to connect to server. Please  try after some time.";
homePage = HomePage;
loginForm: FormGroup;
requestInProgress: boolean= false;
countries: any[]= [];
 constructor(private navCtrl: NavController,fb: FormBuilder,private userService: UserService,public alertCtrl: AlertController,private sim: Sim, private googlePlus: GooglePlus, private facebook: Facebook) {
    
     this.loginForm= fb.group({
         mobile: ['',Validators.compose([Validators.maxLength(14),Validators.minLength(7),Validators.required])],
         country: ['',Validators.required]
    });
  }

  ngOnInit() {
    this.countries = AppConstants.countries;
    this.sim.getSimInfo().then(
      (info) => { 
                  let countryISOCode: string=  info.countryCode;
                  if(AppConstants.DIAL_CODES[countryISOCode.toUpperCase()]){
                    let countryCode= countryISOCode.toUpperCase();
                    this.loginForm.controls.country.setValue(countryCode);
                  }
                  
                },
      (err) => console.log('Unable to get sim info: ', err)
    );
   }

  showRequestInProgress(status: boolean){
    this.requestInProgress= status;
  } 

  showError(msg: string) {
   const alert = this.alertCtrl.create({
    title: 'Error',
    subTitle: msg,
    buttons: ['Dismiss']
   });
   alert.present();
 }

 showOTPDialog(otp_session: string){
    const alert = this.alertCtrl.create({
    title: 'One Time Password',
    inputs: [
      {
        name: 'otp',
        placeholder: 'OTP'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: data => {
          if(!data.otp)   return;
          this.showRequestInProgress(true);  
          this.userService.verifyOTP(this.loginForm.value.mobile,this.loginForm.value.country,data.otp,otp_session).subscribe(data=>{
            this.showRequestInProgress(false);  
            if(data.error){
                alert.setMessage(data.error);
               return false;
           }
           if(data.hasOwnProperty('user') && data.hasOwnProperty('cookie')){
            let user: User= data.user; 
            this.userService.saveUser(user);
            this.userService.saveAuthCookie(data.cookie);
            alert.dismiss();
            if(!user.firstname || !user.email){
              this.navCtrl.setRoot(ProfilePage);
            } else{
              this.navCtrl.setRoot(HomePage);
            }
            
           }
           else{
            alert.dismiss();
            this.showError(LoginPage.JSON_ERROR); 
           }

           
           
          },err=>{
               let errorResponse= JSON.parse(err._body);
               if(errorResponse.error)
                {
                       alert.setMessage(data.error);
                        return false;
                }
          });
          return false;
        }
      }
    ]
  });
  alert.present();
 }

  public login(valid: boolean){
      if(!valid)
        return false;
      this.showRequestInProgress(true);  
      this.userService.doLogin(this.loginForm.value.mobile,this.loginForm.value.country).subscribe(data=>{
          this.showRequestInProgress(false);  
          if(data.otp_session)
               this.showOTPDialog(data.otp_session);
           if(data.error){
               this.showError(data.error); 
               return;
           }
          

      },err=>
      {
        this.showRequestInProgress(false);  
        try{
            let errorResponse= JSON.parse(err._body);
            if(errorResponse.error)
           this.showError(errorResponse.error);
        }  catch(error){
          this.checkForResponseError(err);
        }
        
           
      });
  }

  checkForResponseError(err: any){ 
    try{
      let errorResponse= JSON.parse(err._body);
      if(errorResponse.code=="rest_post_invalid_page_number")
        return;
      if(errorResponse.error=="session_expired"){
          this.showAlert("Seems your session is expired. Please login again.");
          //this.navCtrl.push(LoginPage);
          this.userService.logout();
      }
      else
      this.showAlert(errorResponse.error);   
    }  catch(error){
       this.showAlert('Unable to connect to server. Please  try after some time or check your internet connection.'); 
    }
  }

  showAlert(msg: string,title:string="Error") {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: ['Dismiss']
    });
    alert.present();
   }

   loginWithGoogle() {
    this.requestInProgress=true;
    this.googlePlus.login({
      'webClientId': '355295706725-jju0h51dqmnabbddljlnvr9nkegki8c2.apps.googleusercontent.com',
      'offline': true
    }).then(res => {
      this.userService.registerUser(res.email,res.email,res.givenName).subscribe(response=>{
        this.requestInProgress=false;
        this.socialAccountRegistered(response);
      });
    }, err => {
      this.requestInProgress=false;

      //this.showAlert(err);
    });
   }
   loginWithFb() {
     console.log('loginWithFb');
     this.facebook.getLoginStatus()
     .then((res: FacebookLoginResponse) => {
        if(res && res.status == "connected") 
        {  
          this.fetchUserDataFromFB();
        } 
        else if(res && (res.status == "not_authorized" || res.status == "unknown")) 
        { this.requestInProgress=true;
          this.facebook.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
            this.fetchUserDataFromFB();
          })
          .catch(e => {
            this.requestInProgress=false;
            this.showAlert('Unable to log into Facebook.');
          });
        }
    })
      .catch(e => {
        this.requestInProgress=false;
        this.showAlert(e);
      });
   }

   fetchUserDataFromFB(){
      this.facebook.api('me?fields=id,name,email,first_name', []).then(profile => {
        this.userService.registerUser(profile['email'],profile['email'],profile['first_name']).subscribe(response=>{
          this.requestInProgress=false;
          this.socialAccountRegistered(response);
        });
      }).catch(e=>{
        this.requestInProgress=false;
        this.showAlert(e);
      });
   }

   socialAccountRegistered(response:any){
    if(response.success){
      let user: User= response.data; 
      this.userService.saveUser(user);
      this.userService.saveAuthCookie(response.data.cookie);  
      if(!user.firstname || !user.email){
        this.navCtrl.setRoot(ProfilePage);
      } else{
        this.navCtrl.setRoot(HomePage);
      }
    } 
    else{
      this.showAlert(response.data.msg);
    } 
    
   }
}

