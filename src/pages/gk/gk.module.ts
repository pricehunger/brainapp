import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GKPage } from './gk';

@NgModule({
  declarations: [
    GKPage,
  ],
  imports: [
    IonicPageModule.forChild(GKPage)
  ],
  providers:[]
})
export class GKPageModule {}
