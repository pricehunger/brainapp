import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LangListPage } from '../lang-list/lang-list';

@IonicPage()
@Component({
  selector: 'page-lang-cat',
  templateUrl: 'lang-cat.html',
  providers: []
})

export class LangCatPage{
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }

  openElPlusPage(postType,heading){
    this.navCtrl.push(LangListPage,{type:postType,heading:heading});
  }
}
