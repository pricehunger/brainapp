import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LangCatPage } from './lang-cat';
import { LangListPageModule } from './../lang-list/lang-list-module';

@NgModule({
  declarations: [
    LangCatPage
  ],
  imports: [LangListPageModule,IonicPageModule.forChild(LangCatPage),
  ],
  providers: []
})
export class LangCatPageModule {}
