import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Trick} from '../../models/trick.model';
import {DomSanitizer} from '@angular/platform-browser';
import {UserService} from '../../services/user.service';
import { AlertController } from 'ionic-angular';
//import {LoginPage} from '../login/login';
import {AppConstants} from '../../shared/app.constants';
import {FormGroup,FormBuilder,Validators, RequiredValidator} from '@angular/forms';
import { ImageViewerController } from 'ionic-img-viewer';
import {PostService} from '../../services/post.service';

@IonicPage()
@Component({
  selector: 'page-trick',
  templateUrl: 'trick.html',
  providers: [
    UserService,
    PostService
  ]
})
export class TrickPage implements OnInit{
  trick: Trick;
  userFavorites: any[]=[];
  playForm: FormGroup;
  result:any= null;

  constructor(postService: PostService,public navCtrl: NavController, public navParams: NavParams,private sanitizer: DomSanitizer,private userService: UserService,private alertCtrl: AlertController,fb:FormBuilder,private imageViewerCtrl: ImageViewerController) {
     this.trick= this.navParams.get('trick');
     let loadFromServer= this.navParams.get('loadFromServer');
     
     if(loadFromServer){
      postService.getPost(this.trick.id).subscribe(trick=>{
          this.trick= trick;
        });
     }

     this.playForm= fb.group({
      firstNumber: ['',Validators.compose([Validators.required])],
      secondNumber: ['',Validators.required],
     });
    
  }
  
  getVideoURL(){
    return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + this.trick.video + "?ecver=1");
  }

  ngOnInit(){
      this.userService.getUser().then(user=>{
      this.userFavorites= user.favorites.toString().split(',').map(Number);
      });
  }

  addFavorite(id: number){
    this.userFavorites.push(id);
    this.updateFavorites();
    
 }

 
 showAlert(msg: string,title:string="Error") {
  const alert = this.alertCtrl.create({
   title: title,
   subTitle: msg,
   buttons: ['Dismiss']
  });
  alert.present();
 }

 checkForResponseError(err: any){ 
  try{
    let errorResponse= JSON.parse(err._body);
    if(errorResponse.code=="rest_post_invalid_page_number")
      return;
    if(errorResponse.error=="session_expired"){
        this.showAlert("Seems your session is expired. Please login again.");
       // this.navCtrl.push(LoginPage);
        this.userService.logout();
    }
    else
    this.showAlert(errorResponse.error);   
  }  catch(error){
    this.showAlert(AppConstants.JSON_ERROR); 
  }
}

 updateFavorites(){
   this.userService.getAuthCookie().then(val=>{
   this.userService.saveFavorites(this.userFavorites,val).subscribe((data)=>{
       if(data.hasOwnProperty('user')){
           this.userService.saveUser(data.user);
       }
     },err=>{
       this.checkForResponseError(err);
     });
   })
 }

 removeFavorite(id: number){
   let index= this.userFavorites.indexOf(id);
   if(index>=0)
   { 
     this.userFavorites.splice(index, 1);
     this.updateFavorites();
   }
 }

 calculate(valid){
  if(!valid)
      return;
  let firstNumber= parseInt(this.playForm.controls['firstNumber'].value);       
  let secondNumber= parseInt(this.playForm.controls['secondNumber'].value);  
  if(firstNumber===NaN || secondNumber===NaN){
    this.result = 'Invalid Input';
    return;
  }
  this.result=  eval(firstNumber + this.trick.expression + secondNumber);
}


showCover(coverImage){
  const imageViewer = this.imageViewerCtrl.create(coverImage);
  imageViewer.present();
}
}
