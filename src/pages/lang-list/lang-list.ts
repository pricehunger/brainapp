import { Component,OnInit } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {PostService} from '../../services/post.service';
import { AlertController } from 'ionic-angular';
import {AppConstants} from '../../shared/app.constants';
import { Post } from '../../models/post.model';
import { LangPage } from '../lang/lang';

@Component({
  selector: 'page-languages',
  templateUrl: 'lang-list.html',
  providers:[
    PostService,
    UserService
  ]
})
export class LangListPage implements OnInit{
  user:User;
  posts: Post[]=[];
  searchKeyword: string;
  requestInProgress: boolean= false;
  suggestions: Post[]=[];
  userFavorites: any[]=[];
  postType:string;
  heading: string;

  constructor(private navCtrl: NavController,private service: PostService,private alertCtrl: AlertController,private userService: UserService,private navParams: NavParams) {
    this.postType = navParams.get('type');
    this.heading = navParams.get('heading');
  }

  ngOnInit(){ 
    this.userService.getUser().then(user=>{
      if(user && user.favorites)
        this.userFavorites= user.favorites.toString().split(',').map(Number);
    });
    this.requestInProgress=true;
    this.service.getPosts(1,this.postType).finally(()=>this.requestInProgress=false).subscribe((posts:Post[])=> this.posts= posts);
  }

  doInfinite(infiniteScroll) {
    let page: number= Math.ceil(this.posts.length/5) + 1; 
    this.service.getPosts(page,this.postType).subscribe((posts:Post[])=> { this.posts= this.posts.concat(posts); infiniteScroll.complete() },
    err=> {infiniteScroll.complete(); this.checkForResponseError(err);}
    );
   }

   checkForResponseError(err: any){ 
    try{
      let errorResponse= JSON.parse(err._body);
      if(errorResponse.code=="rest_post_invalid_page_number")
        return;
      if(errorResponse.error=="session_expired"){
          this.showAlert("Seems your session is expired. Please login again.");
          //this.navCtrl.push(LoginPage);
          this.userService.logout();
      }
      else
      this.showAlert(errorResponse.error);   
    }  catch(error){
       this.showAlert(AppConstants.JSON_ERROR); 
    }
  }

  showAlert(msg: string,title:string="Error") {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: ['Dismiss']
    });
    alert.present();
   }

   openDetailPage(lang: Post){
    this.navCtrl.push(LangPage,{'post': lang});
  }

  searchPost(){
    if(this.searchKeyword.length<3){
     this.clearSearch();
     return false;
    }
       
    this.service.searchPosts(this.searchKeyword,this.postType).subscribe((posts:Post[])=>{
       this.suggestions= posts;
    },err=> this.checkForResponseError(err))
  }
 
  onInput(event: any){
     this.searchPost();
  }
 
  clearSearch(){
    this.suggestions= [];
  }


addFavorite(id: number){
  this.userFavorites.push(id);
  this.updateFavorites();
  
}

updateFavorites(){
 this.userService.getAuthCookie().then(val=>{
 this.userService.saveFavorites(this.userFavorites,val).subscribe((data)=>{
     if(data.hasOwnProperty('user')){
         this.userService.saveUser(data.user);
     }
   },err=>{
     this.checkForResponseError(err);
   });
 })
}

removeFavorite(id: number){
 let index= this.userFavorites.indexOf(id);
 if(index>=0)
 { 
   this.userFavorites.splice(index, 1);
   this.updateFavorites();
 }
}

}
