import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LangListPage } from './lang-list';
import { LangPageModule } from '../lang/lang.module';

@NgModule({
  declarations: [
    LangListPage
  ],
  imports: [LangPageModule,IonicPageModule.forChild(LangListPage)],
  providers: []
})
export class LangListPageModule {}
