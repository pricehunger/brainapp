import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GKsPage } from './gks';
import {PostService} from '../../services/post.service';



@NgModule({
  declarations: [
    GKsPage
  ],
  imports: [
    IonicPageModule.forChild(GKsPage),
  ],
  providers:[
    PostService,
  ]
})
export class GKsPageModule {}
