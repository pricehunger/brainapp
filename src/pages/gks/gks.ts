import { Component,OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {QA} from '../../models/qa.model';
import {GKPage} from '../gk/gk';
import {PostService} from '../../services/post.service';
import { AlertController } from 'ionic-angular';
import {AppConstants} from '../../shared/app.constants';

@Component({
  selector: 'page-gks',
  templateUrl: 'gks.html',
  providers:[
    PostService,
    UserService
  ]
})
export class GKsPage implements OnInit  {
  
  user:User;
  gks: QA[]=[];
  searchKeyword: string;
  requestInProgress: boolean= false;
  suggestions: QA[]=[];
  userFavorites: any[]=[];

  constructor(private navCtrl: NavController,private service: PostService,private alertCtrl: AlertController,private userService: UserService) {
    
  }

  ngOnInit(){ 
    this.userService.getUser().then(user=>{
      if(user && user.favorites)
        this.userFavorites= user.favorites.toString().split(',').map(Number);
    });
    this.requestInProgress=true;
    this.service.getPosts(1,'gk').finally(()=>this.requestInProgress=false)
                                 .subscribe((gks:QA[])=> this.gks= gks);
  }

  doInfinite(infiniteScroll) {
    let page: number= Math.ceil(this.gks.length/5) + 1; 
    this.service.getPosts(page,'gk').subscribe((gks:QA[])=> { this.gks= this.gks.concat(gks); infiniteScroll.complete() },
    err=> {infiniteScroll.complete(); this.checkForResponseError(err);}
    );
   }

   checkForResponseError(err: any){ 
    try{
      let errorResponse= JSON.parse(err._body);
      if(errorResponse.code=="rest_post_invalid_page_number")
        return;
      if(errorResponse.error=="session_expired"){
          this.showAlert("Seems your session is expired. Please login again.");
          //this.navCtrl.push(LoginPage);
          this.userService.logout();
      }
      else
      this.showAlert(errorResponse.error);   
    }  catch(error){
       this.showAlert(AppConstants.JSON_ERROR); 
    }
  }

  showAlert(msg: string,title:string="Error") {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: ['Dismiss']
    });
    alert.present();
   }

   openDetailPage(gk: QA){
    this.navCtrl.push(GKPage,{'gk': gk});
  }

  searchPost(){
    if(this.searchKeyword.length<3){
     this.clearSearch();
     return false;
    }
       
    this.service.searchPosts(this.searchKeyword,'gk').subscribe((gks:QA[])=>{
       this.suggestions= gks;
    },err=> this.checkForResponseError(err))
  }
 
  onInput(event: any){
     this.searchPost();
  }
 
  clearSearch(){
    this.suggestions= [];
  }


addFavorite(id: number){
  this.userFavorites.push(id);
  this.updateFavorites();
  
}

updateFavorites(){
 this.userService.getAuthCookie().then(val=>{
 this.userService.saveFavorites(this.userFavorites,val).subscribe((data)=>{
     if(data.hasOwnProperty('user')){
         this.userService.saveUser(data.user);
     }
   },err=>{
     this.checkForResponseError(err);
   });
 })
}

removeFavorite(id: number){
 let index= this.userFavorites.indexOf(id);
 if(index>=0)
 { 
   this.userFavorites.splice(index, 1);
   this.updateFavorites();
 }
}

}
