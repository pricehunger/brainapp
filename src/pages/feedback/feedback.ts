import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import {User} from '../../models/user.model';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import {AppConstants} from '../../shared/app.constants';
import { ValidateEmail } from '../../shared/validators/email.validator';
import {UtilService} from '../../services/util.service';
import {HomePage} from '../home/home';

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
  providers: [
    UserService,
    UtilService
  ]
})

export class FeedbackPage implements OnInit{
  
  feedbackForm: FormGroup;
  user: User;
  requestInProgress: boolean= false;

  constructor(private alertCtrl: AlertController,private storage: Storage ,public navCtrl: NavController, public navParams: NavParams,private userService: UserService,private fb: FormBuilder,private utilService: UtilService) {
  }

  
  ngOnInit(){
    this.feedbackForm= this.fb.group({
      name: ['',Validators.compose([Validators.required])],
      message: ['',Validators.compose([Validators.required])]
    });

    this.userService.getUser().then(user=>{
        this.user= user;
        this.initializeData(user)
    });
  }

  initializeData(user: User){
    this.feedbackForm.controls.name.setValue(user.firstname);
  }

  showRequestInProgress(status: boolean){
    this.requestInProgress= status;
  }

  showAlert(msg: string,title:string="Error") {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: [
      {
        text: 'Dismiss',
        role: 'dismiss',
        handler: () => {
            this.navCtrl.setRoot(HomePage);
        }
      }]
    });
    alert.present();
  }




  send(valid){ 
    if(!valid)
        return;

     let name= this.feedbackForm.get('name').value;   
     let message= this.feedbackForm.get('message').value; 
     this.showRequestInProgress(true);
     this.utilService.sendFeedback(name,message,this.user).subscribe(data=>{
        this.showAlert('Thank you for your valuable feedback.',''); 
        this.showRequestInProgress(false);
     });
  }
}
