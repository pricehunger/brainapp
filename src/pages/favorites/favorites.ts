import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {PostService} from '../../services/post.service';
import {TrickPage} from '../trick/trick';
import {GKPage} from '../gk/gk';
import {ElPlusPage} from '../el-plus/el-plus';
import { AlertController } from 'ionic-angular';
import {UserService} from '../../services/user.service';
import {LoginPage} from '../login/login';
import {AppConstants} from '../../shared/app.constants';
import { LangPage } from '../lang/lang';

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
  providers:[
    PostService,
    UserService
  ]
})

export class FavoritesPage implements OnInit{
  posts: any[]=[];
  postSuggestions: any[]=[];
  searchKeyword: string;
  userFavorites: any[]=[];
  requestInProgress: boolean= false;

 constructor(private userService: UserService,private navCtrl: NavController,private service: PostService,private alertCtrl: AlertController){

 }

  ngOnInit(){
    this.userService.getUser().then(user=>{
      let ids= user.favorites.toString().split(',');
      this.userFavorites= ids.map(Number);
      this.showRequestInProgress(true);
      this.service.getPostsByIds(user.favorites).subscribe(posts=>{
        this.showRequestInProgress(false);
        this.posts= posts;
      },err=>{ 
        this.showRequestInProgress(false); 
        this.checkForResponseError(err); 
      }); 
     });
     
  }

  
  showRequestInProgress(status: boolean){
    this.requestInProgress= status;
  }

  showAlert(msg: string,title:string="Error") {
    const alert = this.alertCtrl.create({
     title: title,
     subTitle: msg,
     buttons: ['Dismiss']
    });
    alert.present();
   }
   
  checkForResponseError(err: any){
    try{
      let errorResponse= JSON.parse(err._body);
      if(errorResponse.code=="rest_post_invalid_page_number")
        return;
      if(errorResponse.error=="session_expired"){
          this.showAlert("Seems your session is expired. Please login again.");
          this.navCtrl.push(LoginPage)
      }
      else
      this.showAlert(errorResponse.error);   
    }  catch(error){
      this.showAlert(AppConstants.JSON_ERROR); 
    }
  }
  
  openDetailPage(post: any){
    if(post.type=='tricks'){
      this.navCtrl.push(TrickPage,{'trick': post,'loadFromServer': true});
    }
    else if(post.type=='general_knowledge'){
      this.navCtrl.push(GKPage,{'gk': post,'loadFromServer': true});
    }
    else if(post.type=='11_plus'){
      this.navCtrl.push(ElPlusPage,{'post': post,'loadFromServer': true});
    }
    else if(['spanish','english','french','hindi'].indexOf(post.type)>=0){
      this.navCtrl.push(LangPage,{'post': post,'loadFromServer': true});
    }
    
  }

 searchTrick(){
   if(this.searchKeyword.length<3)
      return false;
   this.service.searchPosts(this.searchKeyword).subscribe(posts=>{
      this.postSuggestions= posts;
   },err=> this.checkForResponseError(err))
 }

 onInput(event: any){
    this.searchTrick();
 }

  doInfinite(infiniteScroll) {
   let page: number= Math.ceil(this.posts.length/5) + 1; 
   this.userService.getUser().then(user=>{
      let ids= user.favorites.toString().split(',');
      this.service.getPostsByIds(ids,page).subscribe(tricks=> { this.posts= this.posts.concat(tricks); infiniteScroll.complete() },
      err=> {infiniteScroll.complete(); this.checkForResponseError(err);}
      );
   });
  
  }

  updateFavorites(){
    this.userService.getAuthCookie().then(val=>{
    this.userService.saveFavorites(this.userFavorites,val).subscribe((data)=>{
        if(data.hasOwnProperty('user')){
            this.userService.saveUser(data.user);
        }
      },err=>{
        this.checkForResponseError(err);
      });
    })
  }

  removeFavorite(id: number,itemIndex:number){
    this.posts.splice(itemIndex,1);
    let tmpIndex= this.userFavorites.indexOf(id);
    this.userFavorites.splice(tmpIndex,1);
    this.updateFavorites();
  }

}
