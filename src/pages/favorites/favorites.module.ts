import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {FavoritesPage} from './favorites';
import {UserService} from '../../services/user.service'

@NgModule({
  declarations: [
    FavoritesPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoritesPage),
  ],
  providers: [
    UserService
  ]
})
export class FavoritesPageModule {}
