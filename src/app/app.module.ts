import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule  } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Sim } from '@ionic-native/sim';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicImageViewerModule,ImageViewerController } from 'ionic-img-viewer';
import { InAppPurchase } from '@ionic-native/in-app-purchase';


/* Root Component */
import { MyApp } from './app.component';

/* Custom Modules */
import { TrickPageModule } from '../pages/trick/trick.module';
import { GKPageModule } from '../pages/gk/gk.module';
import { LoginPageModule } from '../pages/login/login.module';
import { ProfilePageModule } from '../pages/profile/profile.module';
import { FeedbackPageModule } from '../pages/feedback/feedback.module';
import { FavoritesPageModule } from '../pages/favorites/favorites.module';
import { GKsPageModule } from '../pages/gks/gks.module';
import { ElPlusCatPageModule } from '../pages/el-plus-cat/el-plus-cat.module';
import { LangCatPageModule } from '../pages/lang-cat/lang-cat.module';
/* Pages */
import { HomePage } from '../pages/home/home';

/* Custom Components */
import { MathsPageModule } from '../pages/maths/maths.module';
import { DataProvider } from '../providers/data/data';

/* Providers */
import { QuizesPageModule } from '../pages/quizes/quizes.module';
import { QuizPageModule } from '../pages/quiz/quiz.module';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    TrickPageModule,
    ElPlusCatPageModule,
    LangCatPageModule,
    LoginPageModule,
    ProfilePageModule,
    FeedbackPageModule,
    FavoritesPageModule,
    GKsPageModule,
    GKPageModule,
    MathsPageModule,
    IonicImageViewerModule,
    QuizesPageModule,
    QuizPageModule,
    IonicModule.forRoot(MyApp,{
      scrollAssist: false, 
      autoFocusAssist: false
  }),HttpModule,IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    HttpModule,
    SplashScreen,
    ImageViewerController,
    Sim,
    InAppPurchase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
  ]
})
export class AppModule {}
