import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import { GKsPage } from '../pages/gks/gks';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { FeedbackPage } from '../pages/feedback/feedback';
import {UserService} from '../services/user.service';
import {FavoritesPage} from '../pages/favorites/favorites';
import { Trick } from '../models/trick.model';
import {AppConstants} from '../shared/app.constants';
import {User} from '../models/user.model';
import {PostService} from '../services/post.service';
import { MathsPage } from '../pages/maths/maths';
import {ElPlusCatPage} from '../pages/el-plus-cat/el-plus-cat';
import { Facebook } from '../../node_modules/@ionic-native/facebook';
import { GooglePlus } from '../../node_modules/@ionic-native/google-plus';
import { LangCatPage } from '../pages/lang-cat/lang-cat';
import { QuizesPage } from '../pages/quizes/quizes';

@Component({
  templateUrl: 'app.html',
  providers:[UserService,PostService,SplashScreen,Facebook,GooglePlus]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  user: User;
  pages: Array<{title: string, component: any,icon: any,type}>;
  searchKeyword: string;
  trickSuggestions: Trick[]=[];

  constructor(public platform: Platform, private postService:PostService,public statusBar: StatusBar, public splashScreen: SplashScreen,public storage: Storage,private userService: UserService,private alertCtrl: AlertController,private facebook:Facebook,private googlePlus: GooglePlus) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, icon: 'home' ,type:'home'},
      { title: 'Math Tricks', component: MathsPage, icon: 'calculator',type:'tricks' },
      { title: 'Languages', component: LangCatPage, icon: 'calculator',type:'lang' },
      { title: 'General Knowledge', component: GKsPage, icon: 'book' ,type:'gk'},
      {title: '11+ Maths',component: ElPlusCatPage,icon: 'calculator',type:'maths_11'},
      {title: '11+ English',component: ElPlusCatPage,icon: 'text',type:'english_11'},
      {title: '11+ Science',component: ElPlusCatPage,icon: 'planet',type:'science_11'},
     /* {title: 'Quiz',component: QuizesPage,icon: 'planet',type:'Quiz'},*/
      { title: 'Profile', component: ProfilePage, icon: 'contact',type:'profile' },
      { title: 'Favourites', component: FavoritesPage, icon: 'heart',type:'fav' },
      { title: 'Feedback', component: FeedbackPage, icon: 'ios-paper',type:'feedback' },
      { title: 'Logout', component: ProfilePage, icon: 'log-out',type:'logout' },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
      this.storage.get("authCookie").then(val=>{
          if(val){
              this.userService.getUser().then(user=>{
                this.user= user;
                if((!this.user.firstname && !this.user.lastname) || !this.user.email){
                  this.rootPage= ProfilePage;
                }
                else{
                  this.rootPage= HomePage;
                }
              });
          }
          else{
            this.rootPage= LoginPage;
            
          }
      });
      
    });
  }

  openPage(page) {
    if(page.title=="Logout"){
      this.logout();
      return;
    }
    this.nav.setRoot(page.component,{type: page.type});
  }

  logout(){
    this.userService.logout();
    this.nav.setRoot(LoginPage);
  }

  searchTrick(){
    if(this.searchKeyword.length<3){
     this.clearSearch();
     return false;
    }
       
    this.postService.searchPosts(this.searchKeyword).subscribe(tricks=>{
       this.trickSuggestions= tricks;
    },err=> this.checkForResponseError(err))
  }

  onInput(event: any){
    this.searchTrick();
 }

 showAlert(msg: string,title:string="Error") {
  const alert = this.alertCtrl.create({
   title: title,
   subTitle: msg,
   buttons: ['Dismiss']
  });
  alert.present();
 }

 checkForResponseError(err: any){ 
  try{
    let errorResponse= JSON.parse(err._body);
    if(errorResponse.code=="rest_post_invalid_page_number")
      return;
    if(errorResponse.error=="session_expired"){
        this.showAlert("Seems your session is expired. Please login again.");
        this.nav.push(LoginPage);
        this.userService.logout();
    }
    else
    this.showAlert(errorResponse.error);   
  }  catch(error){
    this.showAlert(AppConstants.JSON_ERROR); 
  }
}

 clearSearch(){
   this.trickSuggestions= [];
 }

}
