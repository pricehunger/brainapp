import {Injectable} from '@angular/core';
import { Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import {AppConstants} from '../shared/app.constants';

@Injectable()
export class QuizService{

    constructor(private http: Http){

    }

    public getQuizes():Observable<any>{
        return this.http.get(AppConstants.QUIZ_URL + 'quiz_names').map(data=> data.json());
    }
    
    public getQuestions(id):Observable<any>{
        return this.http.get(AppConstants.QUIZ_URL + 'questions/' + id).map(data=> data.json());
    }

}
