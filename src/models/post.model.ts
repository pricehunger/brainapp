
export interface Post{
    id: number;
    title: any;
    content: string;
    cover_image: string;
}